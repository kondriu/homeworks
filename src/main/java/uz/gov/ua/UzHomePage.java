package uz.gov.ua;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class UzHomePage {

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    @FindBy(xpath = "//div[@id='e-kvytok-place']")
    private WebElement ticketFrame;

    @FindBy(xpath = "//input[@id='date_dep']")
    private WebElement dateSelect;

    @FindBy(xpath = "//span[@class='ui-datepicker-month']")
    private WebElement selectedMonth;

    @FindBy(xpath = "//span[@class='ui-datepicker-year']")
    private WebElement selectedYear;

    @FindBy(xpath = "//td[@class='  ui-datepicker-current-day']")
    private WebElement selectedDate;

    @FindBys({
            @FindBy(xpath = "//td[@data-handler='selectDay']/a")
    })
    private List<WebElement> calendarDays;

    @FindBy(xpath = "//td[@data-handler='selectDay']//a[@class='ui-state-default ui-state-highlight ui-state-active']")
    private WebElement today;

    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    private WebElement nextMonth;

    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-w']")
    private WebElement previousMonth;


    public UzHomePage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, 30);
        PageFactory.initElements(webDriver, this);
        waitForPageLoaded();
    }


    public void openDatepicker() {
        dateSelect.click();
    }

    public String getSelectedMonth() {
        return selectedMonth.getText();
    }

    public String getSelectedYear() {
        return selectedYear.getText();
    }

    public String getToday() {
        return today.getText();
    }

    public void waitFrame() {
        webDriverWait.until(ExpectedConditions.visibilityOf(ticketFrame));
        System.out.println("...");

    }

    public void clickOnDay(int day) {
        final int dayIndex = day - 1;
        getCalendarDays().get(dayIndex).click();
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(webDriver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    public List<WebElement> getCalendarDays() {
        final List<WebElement> days = new ArrayList<WebElement>();

        for (WebElement item : calendarDays) {
            days.add(item);
        }

        return days;
    }
}

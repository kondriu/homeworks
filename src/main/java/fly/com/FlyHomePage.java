package fly.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class FlyHomePage {

    private WebDriver webDriver;

    @FindBy(xpath = "//div[@class='col-lg-3 col-md-3 col-sm-3 col-xs-3 depart_dates_for_new_box']/div[@class='calendar_selection_new_cal']")
    private WebElement openChoseDepartDate;

    @FindBy(xpath = "//div[@class='col-lg-3 col-md-3 col-sm-3 col-xs-3 arrive_dates_for_new_box']/div[@class='calendar_selection_new_cal']")
    private WebElement openChoseArriveDate;

    @FindBy(xpath = "//span[@class='ui-datepicker-month']")
    private WebElement selectedMonth;

    @FindBy(xpath = "//span[@class='ui-datepicker-year']")
    private WebElement selectedYear;

    @FindBy(xpath = "//td[@data-handler='selectDay']/a[@class='ui-state-default ui-state-active']")
    private WebElement selectedDay;

    @FindBy(xpath = "//td[@class='  ui-datepicker-today']/a")
    private WebElement today;

    @FindBys({@FindBy(xpath = "//td[@data-handler='selectDay']/a")})
    private List<WebElement> calendarDays;

    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-w']")
    private WebElement prevMonth;

    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    private WebElement nextMonth;

    public FlyHomePage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }


    public void openChoseDepartDateDatePicker() {
        openChoseDepartDate.click();
    }

    public void openChoseArriveDateDatePicker() {
        openChoseArriveDate.click();
    }

    public void clickPreviousMonth(){

    }

    public void clickNextMonth(){

    }

    public String getSelectedMonth() {
        return selectedMonth.getText();
    }

    public String getSelectedYear() {
        return selectedYear.getText();
    }

    public String getSelectedDay() {
        return selectedDay.getText();
    }

    public String getToday() {
        return today.getText();
    }

    public void clickOnDay(int i){
        getCalendarDays().get(i);
    }

    private List<WebElement> getCalendarDays() {
        final List<WebElement> days = new ArrayList<WebElement>();
        for (final WebElement i : calendarDays) {
            final WebElement listItem = i;
            days.add(listItem);
        }
        return days;
    }


}

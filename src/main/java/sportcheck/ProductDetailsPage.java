package sportcheck;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetailsPage {

    public WebDriver webDriver;

    @FindBy(xpath = "//h1[@class='global-page-header__title']\n")
    private WebElement productTitle;


    public ProductDetailsPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }


    public String getProductTitle() {
        final String pTitle = productTitle.getText();
        return pTitle;
    }
}

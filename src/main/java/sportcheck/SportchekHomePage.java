package sportcheck;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class SportchekHomePage {

    private WebDriver webDriver;

    @FindBy(xpath = "//div[@id='rfk_search_container']//input")
    private WebElement searchInput;

    @FindBys({
            @FindBy(xpath = "//*[@class='rfk_product']//div[3]")
    })
    private List<WebElement> quickSearchResult;

    @FindBys({
            @FindBy(xpath = "//*[@class='rfk_product']//a[1]")
    })
    private List<WebElement> quickSearchResultLinks;

    @FindBy(xpath = "//*[@class='header-account__trigger' and contains (text(), 'Sign In / My Account')]")
    private WebElement signInLink;

    @FindBy(xpath = "//a[@class='header-account__sign-in__register__link']")
    private WebElement registerNow;

    @FindBy(xpath = "//input[@type='email' and @name='login' and @placeholder='Email (e.g. name@example.com)']")
    private WebElement registerInputEmail;

    @FindBy(xpath = "//input[@placeholder = 'Confirm Email']")
    private WebElement registerInputConfirmEmail;

    @FindBy(xpath = "//input[@placeholder='Password (six characters minimum)']")
    private WebElement registerInputPassword;

    @FindBy(xpath = "//input[@placeholder='Confirm Password']")
    private WebElement registerInputConfirmPassword;

    @FindBy(xpath = "//input[@id='cardNumberMainRegister']")
    private WebElement registerInputCardNumber;

    @FindBy(xpath = "//div[@class='recaptcha-checkbox-checkmark']")
    private WebElement registerNotRobotCaptcha;

    @FindBy(xpath = "//input[@class='register-form__submit button button_color_red']")
    private WebElement registerClickRegisterButton;

    @FindBy(xpath = "//span[@for='login' and contains (@class, 'validation-error validation-error_show')]")
    private WebElement validationErrorLogin;

    @FindBy(xpath = "//span[@for='loginConfirmation' and contains (@class, 'validation-error validation-error_show')]")
    private WebElement validationErrorLoginConfirmation;

    @FindBy(xpath = "//span[@for='password' and contains (@class, 'validation-error validation-error_show')]")
    private WebElement validationErrorPassword;

    @FindBy(xpath = "//span[@for='confirmPassword' and contains (@class, 'validation-error validation-error_show')]")
    private WebElement validationErrorConfirmPassword;

    @FindBy(xpath = "//span[@for='cardNumberMainRegister' and contains (@class, 'validation-error validation-error_show')]")
    private WebElement validationErrorCardNumberMainRegister;

    @FindBy(xpath = "//span[@for='confirmPassword']")
    private WebElement validationCaptchaErrorMessage;


    public SportchekHomePage(final WebDriver webDriver) {

        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }


    public List<WebElement> getQuickSearchLink() {
        final List<WebElement> listLinks = new ArrayList<WebElement>();


        for (final WebElement element : quickSearchResultLinks) {
            final WebElement link = element;
            listLinks.add(link);
        }
        return listLinks;
    }

    public ProductDetailsPage clickOnItemFromQuickSearchLink(int i) {
        getQuickSearchLink().get(i).click();
        return new ProductDetailsPage(webDriver);
    }

    public void typeSearchText(final String text) {
        searchInput.sendKeys(text);
    }

    public List<String> getQuickSearchResult() {
        final List<String> listQuickSearchResult = new ArrayList<String>();

        for (final WebElement element : quickSearchResult) {
            final String txt = element.getText();
            listQuickSearchResult.add(txt);
        }
        return listQuickSearchResult;
    }

    public void clickSignIn() {
        signInLink.click();
    }

    public void clickRegisterNow() {
        registerNow.click();
    }

    public void setRegisterInputEmail(final String inputEmail) {
        registerInputEmail.sendKeys(inputEmail);
    }

    public void setRegisterInputConfirmEmail(final String confirmEmail) {
        registerInputConfirmEmail.sendKeys(confirmEmail);
    }

    public void setRegisterInputPassword(final String inputPassword) {
        registerInputPassword.sendKeys(inputPassword);
    }

    public void setRegisterInputConfirmPassword(final String confirmPassword) {
        registerInputConfirmPassword.sendKeys(confirmPassword);
    }

    public void setRegisterInputCardNumber(final String cardNumber) {
        registerInputCardNumber.sendKeys(cardNumber);
    }

    public void clickRegisterNotRobotCaptcha() {
        registerNotRobotCaptcha.click();
    }

    public void clickRegisterClickRegisterButton() {
        registerClickRegisterButton.click();
    }

    public String getErrorMessageErrorLogin() {
        return validationErrorLogin.getText();
    }

    public String getErrorMessageErrorLoginConfirmation() {
        return validationErrorLoginConfirmation.getText();
    }

    public String getErrorMessageErrorPassword() {
        return validationErrorPassword.getText();
    }

    public String getErrorMessageErrorConfirmPassword() {
        return validationErrorConfirmPassword.getText();
    }

    public String getErrorMessageErrorCardNumberMainRegister() {
        return validationErrorCardNumberMainRegister.getText();
    }

    public String getExpectedValidationCaptchaErrorMessage() {
        return validationCaptchaErrorMessage.getText();
    }
}

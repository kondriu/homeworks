package hotline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class HotlineAutoMoto {

    private WebDriver webDriver;

    public HotlineAutoMoto(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBys({
            @FindBy(xpath = "//li[@class='item']/span")
    })
    private List<WebElement> categories;


    public List<String> getListCategories() {
        final List<String> listCategories = new ArrayList<String>();

        for (final WebElement element : categories){
            final String listItem = element.getText();
            listCategories.add(listItem);

        }

        return listCategories;
    }
}

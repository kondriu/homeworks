package hotline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HotlineHomePage {

    private WebDriver webDriver;

    public HotlineHomePage(final WebDriver webDriver) {

        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    //Авто и Мото
    @FindBy(xpath = "//li[@class='level-1 auto']/a")
    private WebElement sectionAutoMoto;

    public HotlineAutoMoto clickSectionAutoMoto(){
        sectionAutoMoto.click();
        return new HotlineAutoMoto(webDriver);

    }


}

import hotline.HotlineAutoMoto;
import hotline.HotlineHomePage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class Hw03 extends BaseTest {

    final public String[] ruListCategories = {
            "Автозапчасти",
            "Шины и диски",
            "Автозвук, видео, навигация",
            "Автоэлектрика и электроника",
            "Автохимия, масла",
            "Автооборудование",
            "Тюнинг",
            "Автомобили",
            "Мото, скутеры"
    };

    @Test
    public void checkAutoMotoCategories(){
        final WebDriver driver = getDriver();
        driver.get("https://hotline.ua/");
        final HotlineHomePage homePage = new HotlineHomePage(driver);
        final HotlineAutoMoto pageAutoMoto = homePage.clickSectionAutoMoto();
        ArrayList<String> actualCategories = (ArrayList<String>) pageAutoMoto.getListCategories();

        Assert.assertArrayEquals("Categories not match", ruListCategories, actualCategories.toArray());

        System.out.println(ruListCategories[0]+"\n"+actualCategories.get(0).toString());
    }

}

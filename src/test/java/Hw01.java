import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import sportcheck.ProductDetailsPage;
import sportcheck.SportchekHomePage;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Hw01 {

    private WebDriver driver;


    @Before
    public void setDriver() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @After
    public void endSuite() {
        driver.close();
    }

    @Test
    public void searchNike() {
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.typeSearchText("Nike");
        ArrayList actualSearchResults = (ArrayList) page.getQuickSearchResult();

        final String secondItem = actualSearchResults.get(1).toString();
        System.out.println("look for:\t" + secondItem);

        final ProductDetailsPage pageProduct = page.clickOnItemFromQuickSearchLink(1);
        final String pTitle = pageProduct.getProductTitle();
        System.out.println("found:\t\t" + pTitle);

        Assert.assertEquals("wrong title", secondItem, pTitle);
    }
}

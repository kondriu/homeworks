import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import sportcheck.SportchekHomePage;

public class Hw02 extends BaseTest {

    public final String expectedValidationErrorLogin = "Please enter your email address in this format: name@example.com";
    public final String expectedValidationErrorEmptyEmail = "Email is required.";
    public final String expectedValidationErrorLoginConfirmation = "Email addresses must match.";
    public final String expectedValidationErrorPassword = "Your password must be 6-40 characters long.";
    public final String expectedValidationErrorPasswordRrequired = "Password is required.";
    public final String expectedValidationErrorConfirmPassword = "Passwords must match.";
    public final String expectedValidationErrorCardNumberMainRegister = "Please verify Triangle Rewards™ Account number and try again";
    public final String expectedValidationCaptchaErrorMessage = "Please enter captcha";

    @Test
    public void checkRegistryEmailErrorMessage() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("asdf");
        page.setRegisterInputConfirmEmail("asdf");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("12345678");
        page.setRegisterInputCardNumber("2193884825");//636574 2193884825
        final String actualMessage = page.getErrorMessageErrorLogin();
        System.out.println(expectedValidationErrorLogin);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorLogin, actualMessage);
    }

    @Test
    public void checkRegistryEmptyEmailErrorMessage() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("12345678");
        page.setRegisterInputCardNumber("2193884825");
        final String actualMessage = page.getErrorMessageErrorLogin();
        System.out.println(expectedValidationErrorEmptyEmail);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorEmptyEmail, actualMessage);
    }

    @Test
    public void checkRegistryConfirmEmailErrorMessage() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("tname@example.com");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("12345678");
        page.setRegisterInputCardNumber("2193884825");
        final String actualMessage = page.getErrorMessageErrorLoginConfirmation();
        System.out.println(expectedValidationErrorLoginConfirmation);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorLoginConfirmation, actualMessage);
    }

    @Test
    public void checkRegistryPasswordErrorMessagePasswordRequired() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("");
        page.setRegisterInputConfirmPassword("");
        page.setRegisterInputCardNumber("2193884825");
        final String actualMessage = page.getErrorMessageErrorPassword();
        System.out.println(expectedValidationErrorPasswordRrequired);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorPasswordRrequired, actualMessage);
    }

    @Test
    public void checkRegistryPasswordErrorMessagePassword() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("123");
        page.setRegisterInputConfirmPassword("123");
        page.setRegisterInputCardNumber("2193884825");
        final String actualMessage = page.getErrorMessageErrorPassword();
        System.out.println(expectedValidationErrorPassword);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorPassword, actualMessage);
    }

    @Test
    public void checkRegistryPasswordErrorMessageConfirmPassword() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("1234567890");
        page.setRegisterInputCardNumber("2193884825");
        final String actualMessage = page.getErrorMessageErrorConfirmPassword();
        System.out.println(expectedValidationErrorConfirmPassword);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorConfirmPassword, actualMessage);
    }

    @Test
    public void checkRegistryCardNumberErrorMessage() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("12345678");
        page.setRegisterInputCardNumber("1193884833"); //1193884825
        page.clickRegisterClickRegisterButton();
        final String actualMessage = page.getErrorMessageErrorCardNumberMainRegister();
        System.out.println(expectedValidationErrorCardNumberMainRegister);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationErrorCardNumberMainRegister, actualMessage);
    }

    @Test
    public void checkRegistryCaptchaErrorMessage() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca/");
        final SportchekHomePage page = new SportchekHomePage(driver);
        page.clickSignIn();
        page.clickRegisterNow();
        page.setRegisterInputEmail("name@example.com");
        page.setRegisterInputConfirmEmail("name@example.com");
        page.setRegisterInputPassword("12345678");
        page.setRegisterInputConfirmPassword("12345678");
        page.setRegisterInputCardNumber("1193884825");
        page.clickRegisterClickRegisterButton();
        final String actualMessage = page.getExpectedValidationCaptchaErrorMessage();
        System.out.println(expectedValidationCaptchaErrorMessage);
        System.out.println(actualMessage);
        Assert.assertEquals("эррорро", expectedValidationCaptchaErrorMessage, actualMessage);
    }

}

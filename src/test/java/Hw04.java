import fly.com.FlyHomePage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import uz.gov.ua.UzHomePage;

public class Hw04 extends BaseTest {

    //@Test
    public void Tst() {
        final WebDriver driver = getDriver();
        driver.get("https://www.fly.com/");
        final FlyHomePage homePage = new FlyHomePage(driver);
        homePage.openChoseDepartDateDatePicker();
        System.out.println(homePage.getSelectedYear() + " " + homePage.getSelectedMonth() + " " + homePage.getSelectedDay());

        homePage.openChoseArriveDateDatePicker();
        homePage.clickPreviousMonth();
        System.out.println(homePage.getSelectedYear() + " " + homePage.getSelectedMonth() + " " + homePage.getSelectedDay());
        int nextDay = Integer.parseInt(homePage.getToday()) + 1;
        homePage.clickOnDay(nextDay);
    }


    @Test
    public void UzChoseDay() throws InterruptedException {
        final WebDriver driver= getDriver();
        driver.get("http://uz.gov.ua/");

        System.out.println("ща погодим тупейшим образом");
        Thread.sleep(10000);
        System.out.println("погодили");

        final UzHomePage homePage = new UzHomePage(driver);
//        homePage.waitForPageLoaded();
//        homePage.waitFrame();
        homePage.openDatepicker();
        int curday = Integer.parseInt(homePage.getToday());
        System.out.println(curday + " " + homePage.getSelectedMonth() + " " + homePage.getSelectedYear());
        homePage.clickOnDay(curday+2);
    }

}
